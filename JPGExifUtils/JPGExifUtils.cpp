#include <crtdbg.h>
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include "JPGExifUtils.h"

#define MAX_TAG_CHARS 32
#define MAX_MSG_CHARS 256

typedef struct _tag_container {
	WORD tag_number;
	CCHAR tag_name[MAX_TAG_CHARS];
	WORD format;
}TAG_CONTAINER, *PTAG_CONTAINER;

static const TAG_CONTAINER IFD_TAGS[NUMBER_OF_TAGS] = {
	// IFD0 Tags
	{ 0x8769, "ExifOffset",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x8825, "GPSInfo",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x010e, "ImageDescription",DATA_FORMAT_STRING},
	{ 0x010f, "Make",DATA_FORMAT_STRING},
	{ 0x0110, "Model",DATA_FORMAT_STRING},
	{ 0x0112, "Orientation",DATA_FORMAT_UNSIGNED_SHORT},
	{ 0x011a, "XResolution",DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x011b, "YResolution",DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0128, "ResolutionUnit",DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0131, "Software",DATA_FORMAT_STRING },
	{ 0x0132, "DateTime",DATA_FORMAT_STRING },
	{ 0x013e, "WhitePoint",DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x013f, "PrimaryChromaticities",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0211, "YCbCrCoefficients",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0213, "YCbCrPositioning",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0214, "ReferenceBlackWhite",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x8298, "Copyright",	DATA_FORMAT_STRING },
	//Sub IFD tags
	{ 0x829a, "ExposureTime", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x829d, "FNumber", DATA_FORMAT_UNSIGNED_RATIONAL},
	{ 0x8822, "ExposureProgram", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x8827, "ISOSpeedRatings", DATA_FORMAT_UNSIGNED_SHORT},
	{ 0x9003, "DateTimeOriginal", DATA_FORMAT_STRING},
	{ 0x9004, "DateTimeDigitized", DATA_FORMAT_STRING},
	{ 0x9102, "CompressedBitsPerPixel", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x9201, "ShutterSpeedValue", DATA_FORMAT_SIGNED_RATIONAL },
	{ 0x9202, "ApertureValue", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x9203, "BrightnessValue", DATA_FORMAT_SIGNED_RATIONAL },
	{ 0x9204, "ExposureBiasValue", DATA_FORMAT_SIGNED_RATIONAL },
	{ 0x9205, "MaxApertureValue", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x9206, "SubjectDistance", DATA_FORMAT_SIGNED_RATIONAL },
	{ 0x9207, "MeteringMode", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x9208, "LightSource", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x9209, "Flash", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x920a, "FocalLength", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0xa002, "ExifImageWidth", DATA_FORMAT_UNSIGNED_LONG },
	{ 0xa003, "ExifImageHeight", DATA_FORMAT_UNSIGNED_LONG },
	{ 0xa004, "RelatedSoundFile", DATA_FORMAT_STRING },
	{ 0xa20e, "FocalPlaneXResolution", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0xa20f, "FocalPlaneYResolution", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0xa210, "FocalPlaneResolutionUnit", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0xa217, "SensingMethod", DATA_FORMAT_UNSIGNED_SHORT },
	//IFD 1 Tags
	{ 0x0100, "ImageWidth",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0101, "ImageLength", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0102, "BitsPerSample", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0103, "Compression", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0106, "PhotometricInterpretation", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0111, "StripOffsets", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0115, "SamplesPerPixel", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0116, "RowsPerStrip", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0117, "StripByteConunts", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x011a, "XResolution", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x011b, "YResolution", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x011c, "PlanarConfiguration", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0128, "ResolutionUnit", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0201, "JpegIFOffset", DATA_FORMAT_UNSIGNED_LONG },
	{ 0x0202, "JpegIFByteCount", DATA_FORMAT_UNSIGNED_LONG },
	{ 0x0211, "YCbCrCoefficients", DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0212, "YCbCrSubSampling", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0213, "YCbCrPositioning", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0214, "ReferenceBlackWhite", DATA_FORMAT_UNSIGNED_RATIONAL },
	//Misc tags
	{ 0x00fe, "NewSubfileType",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x00ff, "SubfileType",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x012d, "TransferFunction",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x013b, "Artist",	DATA_FORMAT_STRING },
	{ 0x013d, "Predictor",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0142, "TileWidth",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0143, "TileLength",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x0144, "TileOffsets",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x0145, "TileByteCounts",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x014a, "SubIFDs",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x828d, "CFARepeatPatternDim",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x828e, "CFAPattern",	DATA_FORMAT_UNSIGNED_BYTE },
	{ 0x828f, "BatteryLevel",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x83bb, "IPTC / NAA",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x8824, "SpectralSensitivity",	DATA_FORMAT_STRING },
	{ 0x8829, "Interlace", DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x882a, "TimeZoneOffset",	DATA_FORMAT_SIGNED_SHORT },
	{ 0x882b, "SelfTimerMode",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x920b, "FlashEnergy",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x9211, "ImageNumber",	DATA_FORMAT_UNSIGNED_LONG },
	{ 0x9212, "SecurityClassification",	DATA_FORMAT_STRING },
	{ 0x9213, "ImageHistory",	DATA_FORMAT_STRING },
	{ 0x9214, "SubjectLocation",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0x9215, "ExposureIndex",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x9216, "TIFF / EPStandardID",	DATA_FORMAT_UNSIGNED_BYTE },
	{ 0x9290, "SubSecTime",	DATA_FORMAT_STRING },
	{ 0x9291, "SubSecTimeOriginal",	DATA_FORMAT_STRING },
	{ 0x9292, "SubSecTimeDigitized",	DATA_FORMAT_STRING },
	{ 0xa20b, "FlashEnergy",	DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0xa20c, "SpatialFrequencyResponse",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0xa214, "SubjectLocation",	DATA_FORMAT_UNSIGNED_SHORT },
	{ 0xa215, "ExposureIndex",	DATA_FORMAT_UNSIGNED_RATIONAL }
};
static const TAG_CONTAINER GPS_TAGS[NUMBER_OF_GPS_TAGS] = {
	//GPS Tags
	{ 0x0000, "GPSVersionIF", 	  DATA_FORMAT_UNSIGNED_BYTE },
	{ 0x0001, "GPSLatitudeRef", 	  DATA_FORMAT_STRING },
	{ 0x0002, "GPSLatitude", 	  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0003, "GPSLongitudeRef",   DATA_FORMAT_STRING },
	{ 0x0004, "GPSLongitude", 	  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0005, "GPSAltitudeRef", 	  DATA_FORMAT_UNSIGNED_BYTE },
	{ 0x0006, "GPSAltitude",       DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0007, "GPSTimeStamp", 	  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0008, "GPSSatellites", 	  DATA_FORMAT_STRING },
	{ 0x0009, "GPSStatus", 		  DATA_FORMAT_STRING },
	{ 0x000A, "GPSMeasureMode", 	  DATA_FORMAT_STRING },
	{ 0x000B, "GPSDOP", 			  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x000C, "GPSSpeedRef", 	  DATA_FORMAT_STRING },
	{ 0x000D, "GPSSpeed", 		  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x000E, "GPSTrackRef", 	  DATA_FORMAT_STRING },
	{ 0x000F, "GPSTrack", 		  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0010, "GPSImgDirectionRef",DATA_FORMAT_STRING },
	{ 0x0011, "GPSImgDirection",	  DATA_FORMAT_UNSIGNED_RATIONAL },
	{ 0x0012, "GPSMapDatum", 	  DATA_FORMAT_STRING }
};

///Prints error, for a determined message
DWORD PrintError(PCSTR message) {
	printf("\nError:\n\t %s", message);
	DWORD code = GetLastError();
	if (code != 0)
		printf("\ntGet Last Error Code: %d", code);
	return code;
}

///Converts byte align, to the desired align (reverses byte order)
VOID ConvertByteAlign(PBYTE value, WORD length) {
	PBYTE copy = (PBYTE)malloc(length);
	memcpy(copy, value, length);
	for (int i = 0; i < length; i++) {
		memcpy(value + i, copy + (length - i - 1), sizeof(BYTE));
	}
	free(copy);
}

///Returns true when APP exif mark equals "Exif\0\0" and not "JFIF\0\0"
BOOL IsDataExif(PAPP_DATA data) {
	const char * exif = EXIF_MARK;
	int cmp = strcmp(data->exif_mark, exif);
	return  cmp == 0 && data->empty == 0;
}

DWORD SizeOfValue(PIFD_DIR pDir, PTIFF_HEADER tiff) {
	DWORD number_of_components = pDir->number_of_components;
	WORD data_format = pDir->data_format;
	if (tiff->byte_align == MOTOROLLA_ALIGN) {
		ConvertByteAlign((PBYTE)&data_format, sizeof(WORD));
		ConvertByteAlign((PBYTE)&number_of_components, sizeof(DWORD));
	}

	switch (data_format) {
	case DATA_FORMAT_UNSIGNED_BYTE:
	case DATA_FORMAT_STRING:
	case DATA_FORMAT_SIGNED_BYTE:
	case DATA_FORMAT_UNDEFINED:
		return number_of_components;
	case DATA_FORMAT_UNSIGNED_SHORT:
	case DATA_FORMAT_SIGNED_SHORT:
		return number_of_components * 2;
	case DATA_FORMAT_UNSIGNED_LONG:
	case DATA_FORMAT_SIGNED_LONG:
	case DATA_FORMAT_SINGLE_FLOAT:
		return number_of_components * 4;
	case DATA_FORMAT_UNSIGNED_RATIONAL:
	case DATA_FORMAT_SIGNED_RATIONAL:
	case DATA_FORMAT_DOUBLE_FLOAT:
		return number_of_components * 8;
	}

	return 0;

}

///Returns true when data_or_offset of IFD Directory is a data value and not an offset
BOOL IsDataValue(PIFD_DIR pDir, PTIFF_HEADER tiff) {
	return SizeOfValue(pDir, tiff) < sizeof(DWORD);
}

#define START_OF_IMAGE_MARKER 0xd8ff
#define JFIF_DATA_MARKER 0xe0ff
#define EXIF_DATA_MARKER 0xe1ff

/// Receives start of image marker, and returns pointer to exif data
PAPP_HEADER GetExifData(PVOID header) {
	PAPP_HEADER current;
	if (((PIMAGE_HEADER)header)->soi_marker != START_OF_IMAGE_MARKER)
		return NULL;

	current = (PAPP_HEADER)((PWORD)header + 1);

	if (current->app_marker == JFIF_DATA_MARKER) {
		WORD size = current->app_data.app_length;
		ConvertByteAlign((PBYTE)&size, sizeof(WORD));
		current = (PAPP_HEADER)((PBYTE)&current->app_data + size);
	}

	if (current->app_marker == EXIF_DATA_MARKER) {
		header = current;
		return current;
	}

	return NULL;
}

VOID PrintWord(PIFD_DIR dir, 
		PTIFF_HEADER tiff, 
		DWORD number_of_components,
		PCHAR print) {

	DWORD value_or_offset = dir->value_or_offset;
	BOOL big_endian = tiff->byte_align == MOTOROLLA_ALIGN;

	WORD value;
	if (IsDataValue(dir, tiff)) {
		value = value_or_offset;
		if (big_endian)
			ConvertByteAlign((PBYTE)&value, sizeof(WORD));
		printf(print, value);
		return;
	}
	
	if (big_endian)
		ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
	// If it's not a value, it's an offset
	PWORD current = (PWORD)((PBYTE)tiff + value_or_offset);
	for (int i = 0; i < number_of_components; i++, current++) {
		value = *current;
		if(big_endian)
			ConvertByteAlign((PBYTE)&value, sizeof(WORD));
		printf(print, value);
		if (i < number_of_components - 1)
			printf(",");
	}
}

VOID PrintDword(PIFD_DIR dir,
		PTIFF_HEADER tiff,
		DWORD number_of_components,
		PCHAR print) {


	DWORD value_or_offset = dir->value_or_offset;
	BOOL big_endian = tiff->byte_align == MOTOROLLA_ALIGN;

	if (IsDataValue(dir, tiff)) {
		if (big_endian)
			ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
		printf(print, value_or_offset);
		return;
	}

	if (big_endian)
		ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));

	// If it's not a value, it's an offset
	PBYTE current = (PBYTE)tiff + value_or_offset;
	for (int i = 0; i < number_of_components; i++, current += sizeof(DWORD)) {
		value_or_offset = *current;
		if (big_endian)
			ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
		printf(print, value_or_offset);
		if (i < number_of_components - 1)
			printf(",");
	}
}

VOID PrintRational(PIFD_DIR dir,
	PTIFF_HEADER tiff,
	DWORD number_of_components) {

	DWORD value_or_offset = dir->value_or_offset;
	BOOL big_endian = tiff->byte_align == MOTOROLLA_ALIGN;
	if(big_endian)
		ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));


	LONG denominator, numerator;
	DOUBLE result;

	// If it's not a value, it's an offset
	PDWORD current = (PDWORD)((PBYTE)tiff + value_or_offset);
	for (int i = 0; i < number_of_components; i++, current += 2) {

		numerator = *current;
		denominator = *(current + 1);
		
		if (big_endian) {
			ConvertByteAlign((PBYTE)&denominator, sizeof(LONG));
			ConvertByteAlign((PBYTE)&numerator, sizeof(LONG));
		}
		result = (DOUBLE)numerator / (DOUBLE)denominator;
		printf(" %f", result);
		if (i < number_of_components - 1)
			printf(",");
	}
}

/// Prints value of a IFD directory, first checking if it's value or offset,
/// getting the value and printing that value according to it's type;
VOID PrintValue(PIFD_DIR dir, PTIFF_HEADER tiff) {
	WORD data_format = dir->data_format;
	DWORD number_of_comp = dir->number_of_components;
	DWORD value_or_offset = dir->value_or_offset;
	BOOL isDataValue;
	PBYTE value;

	if (tiff->byte_align == MOTOROLLA_ALIGN) {
		ConvertByteAlign((PBYTE)&data_format, sizeof(WORD));
		ConvertByteAlign((PBYTE)&number_of_comp, sizeof(DWORD));
		ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
	}

	if (isDataValue = IsDataValue(dir, tiff)) {
		value = (PBYTE)&value_or_offset;
	} else 
		value = ((PBYTE)tiff + value_or_offset);

	if (data_format == DATA_FORMAT_STRING) {
		printf(" %s", (PCHAR)value);
		return;
	}

	switch (data_format) {
		case DATA_FORMAT_UNSIGNED_SHORT:
			PrintWord(dir, tiff, number_of_comp, " %o");
			return;
		case DATA_FORMAT_UNSIGNED_LONG:
			PrintDword(dir, tiff, number_of_comp, " %d");
			break;
		case DATA_FORMAT_UNSIGNED_BYTE:
			printf(" %o", *(PBYTE)value);
			break;
		case DATA_FORMAT_UNDEFINED:
			break;
		case DATA_FORMAT_SIGNED_BYTE:
			printf(" %d", *(PBYTE)value);
			break;
		case DATA_FORMAT_SIGNED_SHORT:
			PrintWord(dir, tiff, number_of_comp, " %d");
			break;
		case DATA_FORMAT_SIGNED_LONG:
			PrintDword(dir, tiff, number_of_comp, " %d");
			break;
		case DATA_FORMAT_UNSIGNED_RATIONAL:
		case DATA_FORMAT_SIGNED_RATIONAL:
			PrintRational(dir, tiff, number_of_comp);
			break;
		case DATA_FORMAT_SINGLE_FLOAT:
		case DATA_FORMAT_DOUBLE_FLOAT:
			printf(" %f", value);
			break;
	}
}

///Prints the information of the IFD Directory. 
BOOL PrintIfdTag(PIFD_DIR dir, PTIFF_HEADER tiff, const TAG_CONTAINER *tc, DWORD tag_container_length) {
	WORD current_tag = dir->tag_number;

	if (tiff->byte_align == MOTOROLLA_ALIGN) 
		ConvertByteAlign((PBYTE)&current_tag, sizeof(WORD));

	//Search for tag in the tag array
	for (int i = 0; i < tag_container_length; i++) {

		if (tc[i].tag_number == current_tag) {
			printf("\n%s:", tc[i].tag_name);
			PrintValue(dir, tiff);
			return TRUE;
		}
	}
	return FALSE;
}


///Prints GPS IFD data
DWORD PrintGpsIFD(DWORD offset, PTIFF_HEADER tiff_header) {
	DWORD align_offset = offset;
	PIFD_HEADER ifd_header;
	ifd_header = (PIFD_HEADER)((PBYTE)tiff_header + align_offset);
	WORD number_of_dir = ifd_header->number_of_directories;

	if (tiff_header->byte_align == MOTOROLLA_ALIGN)
		ConvertByteAlign((PBYTE)&number_of_dir, sizeof(WORD));

	PIFD_DIR dir = (PIFD_DIR)((PBYTE)ifd_header + sizeof(IFD_HEADER));
	for (int i = 0; i < number_of_dir; i++, dir++) {
		PrintIfdTag(dir, tiff_header, GPS_TAGS, NUMBER_OF_GPS_TAGS);
	}
	return 1;
}

#define SUBIFD_TAG 0
#define GPS_IFD_TAG 1

///Prints IFD data
DWORD PrintIFD(DWORD offset, PTIFF_HEADER tiff) {

	PIFD_HEADER ifd_header = (PIFD_HEADER)((PBYTE)tiff + offset);
	WORD number_of_dir;
	if (tiff->byte_align == MOTOROLLA_ALIGN) {
		DWORD align_offset = offset;
		ConvertByteAlign((PBYTE)&align_offset, sizeof(DWORD));
		ifd_header = (PIFD_HEADER)((PBYTE)tiff + align_offset);
		number_of_dir = ifd_header->number_of_directories;
		ConvertByteAlign((PBYTE)&number_of_dir, sizeof(WORD));
	}
	else {
		ifd_header = (PIFD_HEADER)((PBYTE)tiff + offset);
		number_of_dir = ifd_header->number_of_directories;
	}
		
	PIFD_DIR dir = (PIFD_DIR)((PBYTE)ifd_header + sizeof(IFD_HEADER));
	for (int i = 0; i < number_of_dir; i++, dir++) {
		WORD tag = dir->tag_number;
		DWORD v_or_off = dir->value_or_offset;
		if (tiff->byte_align == MOTOROLLA_ALIGN) {
			ConvertByteAlign((PBYTE)&tag, sizeof(WORD));
			ConvertByteAlign((PBYTE)&v_or_off, sizeof(DWORD));
		}
		if (tag == IFD_TAGS[SUBIFD_TAG].tag_number)
			PrintIFD(dir->value_or_offset, tiff);
		else if (tag == IFD_TAGS[GPS_IFD_TAG].tag_number)
			PrintGpsIFD(v_or_off, tiff);
		else
			PrintIfdTag(dir, tiff, IFD_TAGS, NUMBER_OF_TAGS);
	}
	return 1;
}

/// Goes through Metadata for the image file and prints the infomation, 
/// if no errors are found.
DWORD PrintExifContinue(HANDLE fileMap) {
	PIMAGE_HEADER exif_start;
	PAPP_HEADER app_header;
	PIFD_HEADER ifd_header;
	PAPP_DATA app_data;
	PTIFF_HEADER tiff_header;

	PVOID fileView = MapViewOfFile(fileMap, FILE_MAP_READ, 0, 0, 0);
	if (GetLastError()) {
		PrintError("MapViewOfFile produced an error:");
		return 0;
	}

	//File view is now exif data
	app_header = GetExifData(fileView);
	if (app_header == NULL) //Not an exif file
		return 0;

	app_data = (PAPP_DATA)(&app_header->app_data);
	tiff_header = (PTIFF_HEADER)((PBYTE)app_data + sizeof(APP_DATA));
	PrintIFD(tiff_header->ifd_offset, tiff_header);
	return 1;
}

///Closes handles.
DWORD EndPrint(HANDLE fileMap, HANDLE file) {
	CloseHandle(fileMap);
	CloseHandle(file);
	return 1;
}

///Starts the print exif metadata for Ansi type applications.
JPGEXIFUTILS_DLL_API VOID JPG_PrintMetadataA(PCHAR fileimage){
	HANDLE file = INVALID_HANDLE_VALUE;
	HANDLE fileMap = INVALID_HANDLE_VALUE;

	file = CreateFileA(fileimage, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (file == INVALID_HANDLE_VALUE) {
		PrintError("ASCII: CreateFileW produced an invalid handle.");
		return;
	}

	fileMap = CreateFileMappingA(file, NULL, PAGE_READONLY, 0, 0, NULL);
	if (fileMap == INVALID_HANDLE_VALUE) {
		CloseHandle(file);
		PrintError("ASCII: CreateFileMapping produced a invalid handle.");
		return;
	}

	DWORD res = PrintExifContinue(fileMap);
	EndPrint(file, fileMap);
}

///Starts the print exif metadata for UNICODE type applications.
JPGEXIFUTILS_DLL_API VOID JPG_PrintMetadataW(PWCHAR fileimage){
	HANDLE file = INVALID_HANDLE_VALUE;
	HANDLE fileMap = INVALID_HANDLE_VALUE;

	file = CreateFileW(fileimage, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (file == INVALID_HANDLE_VALUE) {
		PrintError("Wide: CreateFileW produced an invalid handle.");
		return;
	}

	fileMap = CreateFileMappingW(file, NULL, PAGE_READONLY, 0, 0, NULL);
	if (fileMap == INVALID_HANDLE_VALUE) {
		CloseHandle(file);
		PrintError("Wide: CreateFileMapping produced a invalid handle.");
		return;
	}

	DWORD res = PrintExifContinue(fileMap);
	EndPrint(file, fileMap);
}

VOID ProcessTag(HANDLE filemap, PROCESS_EXIF_TAG processor, LPCVOID ctx) {
	PIMAGE_HEADER exif_start;
	PAPP_HEADER app_header;
	PIFD_HEADER ifd_header;
	PIFD_HEADER sub_ifd = NULL;
	PAPP_DATA app_data;
	PTIFF_HEADER tiff_header;
	BOOL big_endian;

	PVOID fileView = MapViewOfFile(filemap, FILE_MAP_READ, 0, 0, 0);
	if (GetLastError()) {
		PrintError("MapViewOfFile produced an error:");
		return;
	}

	//File view is now exif data
	app_header = GetExifData(fileView);
	if (app_header == NULL) //Not an exif file
		return;

	
	app_data = (PAPP_DATA)(&app_header->app_data);
	tiff_header = (PTIFF_HEADER)((PBYTE)app_data + sizeof(APP_DATA));
	big_endian = tiff_header->byte_align == MOTOROLLA_ALIGN;

	DWORD ifd_offset = tiff_header->ifd_offset;
	if (big_endian)
		ConvertByteAlign((PBYTE)&ifd_offset, sizeof(DWORD));

	ifd_header = (PIFD_HEADER)((PBYTE)tiff_header + ifd_offset);

	PIFD_DIR dir = (PIFD_DIR)((PBYTE)ifd_header + sizeof(IFD_HEADER));
	WORD number_of_directories = ifd_header->number_of_directories;
	if (big_endian)
		ConvertByteAlign((PBYTE)&number_of_directories, sizeof(WORD));

	for (int i = 0; i < number_of_directories; i++, dir++) {
		WORD tag = dir->tag_number;
		WORD data_format = dir->data_format;
		DWORD number_of_components = dir->number_of_components;
		DWORD value_or_offset = dir->value_or_offset;

		if (big_endian) {
			ConvertByteAlign((PBYTE)&tag, sizeof(WORD));
			ConvertByteAlign((PBYTE)&data_format, sizeof(WORD));
			ConvertByteAlign((PBYTE)&number_of_components, sizeof(DWORD));
			ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
		}
		if (IFD_TAGS[SUBIFD_TAG].tag_number == tag) 
			sub_ifd = (PIFD_HEADER)((PBYTE)tiff_header + value_or_offset);
		else {
			PVOID pValue;
			if (IsDataValue(dir, tiff_header))
				pValue = (PVOID)&value_or_offset;
			else
				pValue = (PBYTE)tiff_header + value_or_offset;

			BOOL res = processor(ctx, tag, pValue);
			if (!res) return;
		}
	}

	if (sub_ifd != NULL) {
		PIFD_DIR dir = (PIFD_DIR)((PBYTE)sub_ifd + sizeof(IFD_HEADER));
		for (int i = 0; i < sub_ifd->number_of_directories; i++, dir++) {
			WORD tag = dir->tag_number;
			WORD data_format = dir->data_format;
			DWORD number_of_components = dir->number_of_components;
			DWORD value_or_offset = dir->value_or_offset;

			if (big_endian) {
				ConvertByteAlign((PBYTE)&tag, sizeof(WORD));
				ConvertByteAlign((PBYTE)&data_format, sizeof(WORD));
				ConvertByteAlign((PBYTE)&number_of_components, sizeof(DWORD));
				ConvertByteAlign((PBYTE)&value_or_offset, sizeof(DWORD));
			}
			PVOID pValue;
			if (IsDataValue(dir, tiff_header))
				pValue = (PVOID)&value_or_offset;
			else pValue = (PBYTE)tiff_header + value_or_offset;
			if (!processor(ctx, tag, pValue)) return;
		}
	}

}

JPGEXIFUTILS_DLL_API VOID JPG_ProcessExifTagsA(PCHAR fileimage, PROCESS_EXIF_TAG processor, LPCVOID ctx) {
	HANDLE file = INVALID_HANDLE_VALUE;
	HANDLE fileMap = INVALID_HANDLE_VALUE;

	file = CreateFileA(fileimage, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (file == INVALID_HANDLE_VALUE) {
		PrintError("ASCII: CreateFileW produced an invalid handle.");
		return;
	}

	fileMap = CreateFileMappingA(file, NULL, PAGE_READONLY, 0, 0, NULL);
	if (fileMap == INVALID_HANDLE_VALUE) {
		CloseHandle(file);
		PrintError("ASCII: CreateFileMapping produced a invalid handle.");
		return;
	}

	ProcessTag(fileMap, processor, ctx);
	EndPrint(fileMap, file);
}

JPGEXIFUTILS_DLL_API VOID JPG_ProcessExifTagsW(PWCHAR fileimage, PROCESS_EXIF_TAG processor, LPCVOID ctx) {
	HANDLE file = INVALID_HANDLE_VALUE;
	HANDLE fileMap = INVALID_HANDLE_VALUE;

	file = CreateFileW(fileimage, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (file == INVALID_HANDLE_VALUE) {
		PrintError("Wide: CreateFileW produced an invalid handle.");
		return;
	}

	fileMap = CreateFileMappingW(file, NULL, PAGE_READONLY, 0, 0, NULL);
	if (fileMap == INVALID_HANDLE_VALUE) {
		CloseHandle(file);
		PrintError("Wide: CreateFileMapping produced a invalid handle.");
		return;
	}
	ProcessTag(fileMap, processor, ctx);
	EndPrint(fileMap, file);
}
