#ifndef _JPGEXIFUTILS_H
#define _JPGEXIFUTILS_H

#include <Windows.h>


#define INTEL_ALIGN 0x4949
#define MOTOROLLA_ALIGN 0x4d4d

#define EXIF_MARK_LENGTH 4
#define EXIF_MARK "Exif"

// Data Format types
#define DATA_FORMAT_UNSIGNED_BYTE 1
#define DATA_FORMAT_STRING 2
#define DATA_FORMAT_UNSIGNED_SHORT 3
#define DATA_FORMAT_UNSIGNED_LONG 4
#define DATA_FORMAT_UNSIGNED_RATIONAL 5
#define DATA_FORMAT_SIGNED_BYTE 6
#define DATA_FORMAT_UNDEFINED 7
#define DATA_FORMAT_SIGNED_SHORT 8
#define DATA_FORMAT_SIGNED_LONG 9
#define DATA_FORMAT_SIGNED_RATIONAL 10
#define DATA_FORMAT_SINGLE_FLOAT 11
#define DATA_FORMAT_DOUBLE_FLOAT 12

#define NUMBER_OF_GPS_TAGS 19
#define NUMBER_OF_TAGS 98

// Set unaligned struct fields
#pragma pack(push, 1)
typedef struct _ifd_directory {
	WORD tag_number;			//Tag number, check ifd tag number
	WORD data_format;			//Data format, check data format table
	DWORD number_of_components; //Number of components
	DWORD value_or_offset;		//Data or offset to data
}IFD_DIR, *PIFD_DIR;

///IFD: Image File Directory
typedef struct _ifd_header {
	WORD number_of_directories;
}IFD_HEADER, *PIFD_HEADER;

typedef struct _tiff_header {
	WORD byte_align;		//"II" for intel, or "MM" for motorolla
	WORD tag_mark;			//
	DWORD ifd_offset;		//Offset to IFD, starts counting from empty word
}TIFF_HEADER, *PTIFF_HEADER;

typedef struct _app_data {
	WORD  app_length; // &app_length + 
	CCHAR exif_mark[EXIF_MARK_LENGTH];	//If it's not equal to the ASCII string - "Exif", it's not equal to exif
	WORD  empty;						//Followed by exif_mark it's 0 word
}APP_DATA, *PAPP_DATA;

typedef struct _app_header {
	WORD app_marker;		//Exif marker
	APP_DATA app_data;
}APP_HEADER, *PAPP_HEADER;

typedef struct _exif_header {
	WORD soi_marker;		//Marker that indicates the start of image - 0xFFD8
} IMAGE_HEADER, *PIMAGE_HEADER;

// Unset unaligned struct fields
#pragma pack(pop)

typedef BOOL(*PROCESS_EXIF_TAG)(LPCVOID ctx, DWORD tag, LPVOID value);

#ifdef JPGEXIFUTILS_DLL_EXPORT
#define JPGEXIFUTILS_DLL_API __declspec(dllexport)
#else
#define JPGEXIFUTILS_DLL_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {
#endif

	JPGEXIFUTILS_DLL_API VOID JPG_PrintMetadataA(PCHAR fileimage);
	JPGEXIFUTILS_DLL_API VOID JPG_PrintMetadataW(PWCHAR fileimage);

	/*
	* Call processor for each exif tag in fileimage while processor returns true. Return immediatly after processor returns false.
	*/
	JPGEXIFUTILS_DLL_API VOID JPG_ProcessExifTagsA(PCHAR fileimage, PROCESS_EXIF_TAG processor, LPCVOID ctx);
	JPGEXIFUTILS_DLL_API VOID JPG_ProcessExifTagsW(PWCHAR fileimage, PROCESS_EXIF_TAG processor, LPCVOID ctx);

#ifdef UNICODE
#define JPG_PrintMetadata JPG_PrintMetadataW
#define JPG_ProcessExifTags JPG_ProcessExifTagsW
#else
#define JPG_PrintMetadata JPG_PrintMetadataA
#define JPG_ProcessExifTags JPG_ProcessExifTagsA
#endif

#ifdef __cplusplus
}
#endif

#endif/*_JPGEXIFUTILS_H*/
