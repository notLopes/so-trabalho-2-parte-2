#include "sdtafx.h"

#define MAX_THREADS 2
#define MAX_SWITCH 1000
#define MASK 0x01

DWORD WINAPI ThreadFunc(LPVOID arg) 
{
	for (int i = 0; i < MAX_SWITCH; i++)
	{
		printf("start thread %p\n", arg);
		SwitchToThread();
	}
	return 0;
}

double GetTimeBetweenThreadsSwitches() 
{
	DWORD	dwFuncParams[MAX_THREADS];
	DWORD   dwThreadIdArray[MAX_THREADS];
	HANDLE  hThreadArray[MAX_THREADS];
	int start, end;

	start = GetTickCount();
	for (int i = 0; i<MAX_THREADS; i++)
	{
		dwFuncParams[i] = i;
		hThreadArray[i] = CreateThread(NULL, 0, ThreadFunc, (LPVOID*)dwFuncParams[i], 0, &dwThreadIdArray[i]);
		DWORD dw = SetThreadAffinityMask(hThreadArray[i], MASK);
		SetThreadPriority(hThreadArray[i], 2);

		if (hThreadArray[i] == NULL || dw == 0)
		{
			ExitProcess(3);
		}
	}
	
	WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);
	end = GetTickCount();

	DWORD retValue;
	
	for (int i = 0; i<MAX_THREADS; i++)
	{
		GetExitCodeThread(hThreadArray, &retValue);
		CloseHandle(hThreadArray[i]);
		printf("thread created with id %d returned %o\n", dwThreadIdArray[i], retValue);
	}
	return (double)((end - start)) / (MAX_THREADS*MAX_SWITCH);
}

int main()
{
	double time;
	time = GetTimeBetweenThreadsSwitches();
	printf("The time between threads switches is %f\n", time);
	printf("Choose any key to exit\n");
	getchar();


	return 0;
}