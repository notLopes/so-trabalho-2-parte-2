#include "sdtafx.h"

#define MAX_THREADS 2
#define MAX_SWITCH 1000
#define MASK 0x01

DWORD WINAPI ThreadFunc(LPVOID arg)
{
	for (int i = 0; i < MAX_SWITCH; i++)
	{
		printf("start process 1 thread\n");
		SwitchToThread();
	}
	return 0;
}

HANDLE MyCreateThread()
{
	DWORD   dwThreadId;
	HANDLE  hThread;
	
	hThread = CreateThread(NULL, 0, ThreadFunc, NULL, CREATE_SUSPENDED, &dwThreadId);
	DWORD dw = SetThreadAffinityMask(hThread, MASK);
	SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);

	if (hThread == NULL || dw == 0)
	{
		ExitProcess(3);
	}

	return hThread;
}

PROCESS_INFORMATION MyCreateProcess(LPTSTR cmd) 
{
	printf("Creating process: %s\n\n", cmd);
	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;

	LPTSTR cmdLine = _tcsdup(TEXT("Serie2.exe"));
	if (!CreateProcess(NULL, cmd, NULL, NULL,FALSE, 0, NULL, NULL, &si, &pi))
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return pi;
	}

	SetThreadAffinityMask(pi.hThread, MASK);
	SetThreadPriority(pi.hThread, THREAD_PRIORITY_TIME_CRITICAL);

	return pi;
}

int main(int argc, TCHAR *argv[])
{
	if (argc != 2) 
	{
		printf("Failed.");
	}

	DWORD start, end;
	
	HANDLE thread = MyCreateThread();
	
	LPTSTR cmdLine = _tcsdup(TEXT("Serie2-exe2-1-1.exe"));
	PROCESS_INFORMATION process = MyCreateProcess(cmdLine);


	HANDLE handles[MAX_THREADS] = {thread, process.hThread};

	ResumeThread(thread);
	start = GetTickCount();

	WaitForMultipleObjects(MAX_THREADS, handles,TRUE ,INFINITE);
	
	end = GetTickCount();
	CloseHandle(thread);
	CloseHandle(process.hThread);
	CloseHandle(process.hProcess);

	double time = (double)(end - start) / (MAX_SWITCH*MAX_THREADS);
	printf("The time between threads switches in different process is %f\n", time);

	printf("Choose any key to exit\n");
	getchar();

	return 0;
}