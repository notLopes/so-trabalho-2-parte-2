#include <Windows.h>
#include <stdio.h>
#include <assert.h>
#include <process.h>
#include "../Include/JPG_SearchService.h"
#include "../JPGExifUtils/JPGExifUtils.h"
#include "../JPG_SearchProgram/CountdownLatch.h"

// Struct to hold context to JPG_ProcessExifTags
typedef struct {
	HANDLE mutex; 
	TCHAR filepath[MAX_PATH];				// Name to collect
	PCSTR searchArgs;			// Filter to use in tag comparation
	PTCHAR response;
	PCOUNTDOWN_LATCH latch;
} JPG_CTX, *PJPG_CTX;


BOOL ProcessExifTag(LPCVOID ctx, DWORD tag, LPVOID value) {
	PJPG_CTX pCtx = (PJPG_CTX)ctx;
	PCSTR searchArgs = pCtx->searchArgs;
	PTCHAR response = pCtx->response;
	DWORD tagToFind = atoi((PCHAR)searchArgs);
	BOOL bRes = tagToFind == tag;
	if (bRes) {
		WaitForSingleObject(pCtx->mutex, INFINITE);
		PCSTR filepath = pCtx->filepath;
		size_t currentSize = _tcslen(response);
		currentSize += _tcslen(pCtx->filepath) + 2 * sizeof(TCHAR);
		//response = (PTCHAR)realloc(response, currentSize * sizeof(TCHAR));
		_tcscat_s(response, currentSize, _T("\n"));
		_tcscat_s(response, currentSize, filepath);
		ReleaseMutex(pCtx->mutex);
	}
	return !bRes;
}

unsigned int WINAPI worker_thread(void * ctx) {
	PJPG_CTX pctx = (PJPG_CTX)ctx;
	
	JPG_ProcessExifTags((PTCHAR)pctx->filepath, ProcessExifTag, pctx);
	DecrementCountdownLatch(pctx->latch);
	free(pctx);
	return 0;
}

// @param ctx No need for this implementation.
static PVOID SearchFileDir(PCSTR path, PCSTR searchArgs, LPVOID ctx) {
	PTCHAR answer = (PTCHAR)ctx;
	COUNTDOWN_LATCH latch;
	CHAR buffer[MAX_PATH];		// auxiliary buffer
	// the buffer is needed to define a match string that guarantees 
	// a priori selection for all files
	sprintf_s(buffer, "%s\\%s", path, "*.*");

	WIN32_FIND_DATA fileData;
	HANDLE fileIt = FindFirstFile(buffer, &fileData);
	if (fileIt == INVALID_HANDLE_VALUE) return NULL;
	HANDLE mutex = CreateMutex(NULL, FALSE, NULL);
	InitializeCountdownLatch(0, &latch);

	// Process directory entries
	do {
		if ((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) {
			// Not processing "." and ".." files!
			if (strcmp(fileData.cFileName, ".") && strcmp(fileData.cFileName, "..")) {
				_stprintf_s(buffer, _T("%s\\%s"), path, fileData.cFileName);
				// Recursively process child directory
				SearchFileDir(buffer, searchArgs, ctx);
			}
		}
		else {
			PJPG_CTX jpgCtx = (PJPG_CTX)malloc(sizeof(JPG_CTX));
			jpgCtx->latch = &latch;
			jpgCtx->response = answer;
			jpgCtx->mutex = mutex;
			IncrementCountdownLatch(jpgCtx->latch); 
			jpgCtx->searchArgs = searchArgs;
			// Process file archive
			_stprintf_s(jpgCtx->filepath, "%s\\%s", path, fileData.cFileName);
			_beginthreadex(NULL, 0, &worker_thread, jpgCtx, 0, 0);
		}
	} while (FindNextFile(fileIt, &fileData) == TRUE);

	WaitForSingleObject((&latch)->evt, INFINITE);
	FindClose(fileIt);
	CloseHandle(mutex);
	CloseCountdownLatch(&latch);
	
	return answer;
}

LPVOID ProcessRepository(PCSTR repository, PCSTR filter, PTCHAR answer) {
	printf("\n(TIME: %o)Searching repository: %s", GetTickCount(), repository);
	PVOID res = SearchFileDir(repository, filter, answer);
	printf("\n(TIME: %o)End search on repository: %s", GetTickCount(), repository);
	return res;
}

INT main(INT argc, PCHAR argv[]) {
	if (argc < 2) {
		printf("Use: %s <service name>", argv[0]);
		exit(0);
	}

	PCHAR serviceName = argv[1];

	PJPG_SEARCH_SERVICE service = JPG_SearchServiceCreate(serviceName);
	if (service == NULL) return 0;

	printf("Using %s", argv[1]);
	JPG_SearchServiceProcess(service, ProcessRepository);
	JPG_SearchServiceClose(service);

	return 0;
}