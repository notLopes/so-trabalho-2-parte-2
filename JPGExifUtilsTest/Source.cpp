#include <Windows.h>
#include <stdio.h>
#include "../JPGExifUtils/JPGExifUtils.h"


typedef struct {
	PCTSTR filepath;
	PCTSTR searchArgs;
} JPG_CTX, *PJPG_CTX;

BOOL proc(LPCVOID ctx, DWORD tag, LPVOID value) {
	printf("\n%X", tag);
	return TRUE;
}

DWORD main() {
	PWCHAR image1 = L"..\\Resources\\fugitivemcafee.jpg";
	PWCHAR image2 = L"..\\Resources\\img_2158.jpg";

	JPG_PrintMetadata(image1);
	printf("\n-------");
	JPG_PrintMetadata(image2);
	//JPG_ProcessExifTags(image1, proc, NULL);
	return 1;
}