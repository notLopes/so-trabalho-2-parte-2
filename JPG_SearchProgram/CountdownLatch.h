#pragma once
#include <Windows.h>

typedef struct countdown_latch {
	HANDLE mutex;
	HANDLE evt;
	DWORD countdown;
}COUNTDOWN_LATCH, *PCOUNTDOWN_LATCH;

VOID InitializeCountdownLatch(DWORD counter, __out PCOUNTDOWN_LATCH countdown);

VOID IncrementCountdownLatch(PCOUNTDOWN_LATCH cl);

VOID DecrementCountdownLatch(PCOUNTDOWN_LATCH cl);
VOID CloseCountdownLatch(PCOUNTDOWN_LATCH cl);