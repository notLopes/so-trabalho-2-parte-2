#include "Date.h"


udate GetDate(const PCHAR stringDate) {
	udate date;

	date.year = _ttoi(stringDate);
	date.month = _ttoi(stringDate + 5);
	date.day = _ttoi(stringDate + 8);
	return date;
}

BOOL IsDateBetweenInterval(udate* initial, udate* end, udate* date) {
	BOOL biggerOrEqualThanInitial = (date->year > initial->year ||
		((date->year == initial->year) && (date->month > initial->month ||
		(date->month == initial->month && date->day >= initial->day))));

	BOOL smallerOrEqualThanEnd = (date->year < end->year ||
		((date->year == end->year) && (date->month < end->month ||
		(date->month == end->month && date->day <= end->day))));

	return biggerOrEqualThanInitial && smallerOrEqualThanEnd;
}