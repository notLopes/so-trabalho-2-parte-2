#pragma once

#include <Windows.h>
#include <stdio.h>
#include <tchar.h>

typedef struct udate {
	WORD year;
	BYTE month;
	BYTE day;
};

udate GetDate(PCHAR stringDate);
BOOL IsDateBetweenInterval(udate* initial, udate* end, udate* date);