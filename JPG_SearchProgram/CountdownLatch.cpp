#include <Windows.h>
#include "CountdownLatch.h"

VOID InitializeCountdownLatch(DWORD counter, __out PCOUNTDOWN_LATCH countdown) {
	countdown->countdown = counter;
	countdown->mutex = CreateMutex(NULL, FALSE, NULL);
	if(counter == 0)
		countdown->evt = CreateEvent(NULL, TRUE, TRUE, NULL);
	else 
		countdown->evt = CreateEvent(NULL, TRUE, FALSE, NULL);
}

VOID CloseCountdownLatch(__out PCOUNTDOWN_LATCH countdown) {
	CloseHandle(countdown->evt);
	CloseHandle(countdown->mutex);
}

VOID IncrementCountdownLatch(PCOUNTDOWN_LATCH cl) {
	WaitForSingleObject(cl->mutex, INFINITE);
	cl->countdown += 1;
	ResetEvent(cl->evt);
	ReleaseMutex(cl->mutex);
}


VOID DecrementCountdownLatch(PCOUNTDOWN_LATCH cl) {
	WaitForSingleObject(cl->mutex, INFINITE);
	cl->countdown -= 1;
	if (cl->countdown == 0)
		SetEvent(cl->evt);
	ReleaseMutex(cl->mutex);
}