#include "CountdownLatch.h"
#include "Date.h"
#include "../JPGExifUtils/JPGExifUtils.h"	//Contains most included header files
#include "../Include/PrintUtils.h"

#define DATE_TAKEN_TAG 0x9003
#define MAX_CHARS 255 * 2048
// Struct to hold context to JPG_ProcessExifTags
typedef struct {				
	TCHAR filepath[MAX_PATH];
	PVOID searchArgs;
	BOOL mainThread;
} JPG_CTX, *PJPG_CTX;

countdown_latch answer;
TCHAR response[MAX_CHARS];

DWORD freeThreadsCounter;
HANDLE ftxMutex;

// Callback to JPG_ProcessExifTags
BOOL ProcessExifTag(LPCVOID ctx, DWORD tag, LPVOID value) {
	PJPG_CTX jCtx = ((PJPG_CTX)ctx);
	BOOL bRes = (DATE_TAKEN_TAG == tag);
	if (bRes) {
		udate actual = GetDate((PCHAR)value);
		if (IsDateBetweenInterval((udate*)jCtx->searchArgs, (udate*)jCtx->searchArgs+1, &actual)) {
			WaitForSingleObject(answer.mutex, INFINITE);

			_tcscat_s(response, MAX_CHARS, "\n");
			_tcscat_s(response, MAX_CHARS, jCtx->filepath);

			ReleaseMutex(answer.mutex);
		}
	}
	return !bRes;
}

DWORD WINAPI worker_thread(PVOID ctx) {
	PJPG_CTX pctx = (PJPG_CTX)ctx;
	JPG_ProcessExifTags((PTCHAR)pctx->filepath, ProcessExifTag, pctx);
	DecrementCountdownLatch(&answer);
	
	if (!pctx->mainThread) {
		WaitForSingleObject(ftxMutex, INFINITE);
		freeThreadsCounter++;
		ReleaseMutex(ftxMutex);
	}
	free(pctx);
	return 0;
}

VOID SearchFileDir(PCTSTR path, PVOID searchArgs, LPVOID ctx) {

	TCHAR buffer[MAX_PATH];	
	//PTCHAR buffer = (PTCHAR)malloc(MAX_PATH * sizeof(TCHAR));
	_stprintf_s(buffer, _T("%s%s"), path, _T("\\*.*"));

	WIN32_FIND_DATA fileData;
	HANDLE fileIt = FindFirstFile(buffer, &fileData);
	if (fileIt == NULL) return;

	// Process directory entries
	do {
		if ((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) {
			// Not processing "." and ".." files!
			if (_tcscmp(fileData.cFileName, _T(".")) && _tcscmp(fileData.cFileName, _T(".."))) {
				_stprintf_s(buffer, _T("%s\\%s"), path, fileData.cFileName);
				// Recursively process child directory
				//size_t bufferLen = (_tcsclen(buffer) + 1) * sizeof(TCHAR);
				//PTCHAR dir = (PTCHAR)malloc(bufferLen);
				//memcpy(dir, buffer, bufferLen);
				SearchFileDir(buffer, searchArgs, ctx);
			}
		}
		else {
			IncrementCountdownLatch(&answer);
			PJPG_CTX jpgCtx = (PJPG_CTX)malloc(sizeof(JPG_CTX));
			jpgCtx->searchArgs = searchArgs;
			_stprintf_s(jpgCtx->filepath, "%s\\%s", path, fileData.cFileName);
			// Process file archive
			if(freeThreadsCounter > 0){
				WaitForSingleObject(ftxMutex, INFINITE);
				QueueUserWorkItem(&worker_thread, jpgCtx, WT_EXECUTEDEFAULT);
				freeThreadsCounter--;
				ReleaseMutex(ftxMutex);
			}
			else {
				jpgCtx->mainThread = TRUE;
				worker_thread(jpgCtx);
			}
		}
	} while ( FindNextFile(fileIt, &fileData) == TRUE );

	FindClose(fileIt);
}

DWORD _tmain(DWORD argc, PTCHAR argv[]) {

	if (argc < 2) {
		_tprintf(_T("Use: %s <repository path> <opt date 1> <opt date 2>"), argv[0]);
		exit(0);
	}

	udate dates[2] = { NULL, NULL };
	if (argc > 3){
		dates[0] = GetDate(argv[2]);
		dates[1] = GetDate(argv[3]);
	}
	else if(argc > 2){
		dates[0] = GetDate(argv[2]);
		dates[1] = GetDate("2099:12:30");
	} else {
		dates[0] = GetDate("1970:01:01");
		dates[1] = GetDate("2099:12:30");
	}
	
	
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	
	freeThreadsCounter = si.dwNumberOfProcessors;
	ftxMutex = CreateMutex(NULL, FALSE, NULL);
	//Start countdown latch for the answers
	InitializeCountdownLatch(0, &answer);	//Will be incremented on SearchFileDir

	SearchFileDir(argv[1], (PVOID)dates, NULL);

	WaitForSingleObject(answer.evt, INFINITE);
	CloseCountdownLatch(&answer);
	CloseHandle(ftxMutex);
	_tprintf(response);
	PRESS_TO_FINISH("");
	return 0;
}

